# Personal portfolio

To explore the possibilities of the clouds, I were to create a simple, static personal portfolio app to deploy to a S3 bucket on AWS.
The project is a part of the Clouds course at EURECOM.

## Stack

The application is created with

- Svelte
- Typescript
- TailwindCSS (and postCSS)
